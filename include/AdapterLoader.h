#ifndef ADAPTER_LOADER
#define ADAPTER_LOADER

#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <map>
#include <list>
#include <filesystem>
#include "UpdaterAdapter.h"

#ifdef __cplusplus
extern "C" {
#endif

	typedef adapter_t* (*fn_load)(adapter_callback);
	typedef const char*(*fn_get_identity)(adapter_t*);
	typedef const AdapterKey*(*fn_get_pre_shared_key)(adapter_t*);
	typedef bool(*fn_unload)(adapter_t*);
	typedef bool(*fn_set_security)(adapter_t*, const unsigned char*, unsigned long, const unsigned char*, unsigned long);
	typedef bool(*fn_load_from_buffer)(adapter_t*, const unsigned char*, unsigned long);
	typedef bool(*fn_load_from_dirpath)(adapter_t*, const unsigned char*, unsigned long);
	typedef bool(*fn_apply)(adapter_t*, unsigned long*);
	typedef bool(*fn_rollback)(adapter_t*, unsigned long*);

	typedef struct SoftwareUpdaterAdapter
	{
		HMODULE adapterlib;
		fn_load load;
		fn_get_identity identity;
		fn_get_pre_shared_key pre_shared_key;
		fn_unload unload;
		fn_set_security set_security;
		fn_load_from_buffer load_from_buffer;
		fn_load_from_dirpath load_from_dirpath;
		fn_apply apply;
		fn_rollback rollback;
		AdapterKey* keyptr;
		char* identitystr;
	}SoftwareUpdaterAdapter;

	class AdapterLoader
	{
	public:
		virtual ~AdapterLoader();
		static AdapterLoader* GetInstance();
		unsigned long LoadAdapters();
		unsigned long UnloadAdapters();
		SoftwareUpdaterAdapter* SelectAdapter(std::string &str);
	protected:
		void static OnAdapterNotification(const AdapterNotification* notification);
	private:
		AdapterLoader();
		void ReleaseCurrentAdapter(SoftwareUpdaterAdapter* ptr);
		void GetDllList(const char* dirpath, std::list<std::string> &filelist);
		std::map<std::string, SoftwareUpdaterAdapter*> adapterList;
	};

#ifdef __cplusplus
}
#endif

#endif