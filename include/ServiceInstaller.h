#ifndef SERVICE_INSTALLER
#define SERVICE_INSTALLER

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include <queue>
#include <string>
#include <thread>
#include <mutex>

#include <Windows.h>
#include <tchar.h>

#ifdef __cplusplus
extern "C" {
#endif

	BOOL WINAPI InstallUpdaterService();
	BOOL WINAPI DeleteUpdaterService();
	BOOL WINAPI VerifyCredentials();

#ifdef __cplusplus
}
#endif

#endif