#ifndef MESSAGE_NOTIFIER
#define MESSAGE_NOTIFIER

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

	class MessageNotifier
	{
		public:
			virtual ~MessageNotifier();
			static MessageNotifier* GetInstance();
			bool OpenNotifier();
			bool CloseNotifier();
			bool IsOpen();
			bool SendNotification(const char* lptrmessage);
	private:
			MessageNotifier();
	};

#ifdef __cplusplus
}
#endif

#endif