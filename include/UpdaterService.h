#ifndef UPDATER_SERVICE
#define UPDATER_SERVICE

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include <queue>
#include <string>
#include <thread>
#include <mutex>

#include <Windows.h>
#include <tchar.h>

#ifdef __cplusplus
extern "C" {
#endif

	VOID WINAPI ServiceMain(DWORD argc, LPTSTR* argv);
	VOID WINAPI DebugMain(DWORD argc, LPTSTR* argv);
	DWORD WINAPI ServiceCtrlHandler(DWORD CtrlCode, DWORD evtype, PVOID evdata, PVOID Context);
	DWORD WINAPI DeviceEventNotify(DWORD evtype, PVOID evdata, PVOID context);
	char FirstDriveFromMask(ULONG unitmask);
	void PreLoadDisks();
	VOID WINAPI LogMessage(const char* message);

#ifdef __cplusplus
}
#endif

#endif