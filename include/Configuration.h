#ifndef CONFIGURATION
#define CONFIGURATION

#include <map>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <filesystem>

#ifdef __cplusplus
extern "C" {
#endif

    typedef std::map<std::string, std::string> KeyValueList;

    class Configuration
    {
    public:
        virtual ~Configuration();
        static Configuration* GetInstance();
        void setFileName(std::string fname);
        bool loadConfiguration();
        std::string getValue(const std::string& section, const std::string& settingKey, const std::string defval = "");
        bool isSection(const std::string& section);
    private:
        Configuration();
        bool loadConfiguration(const std::string& configFile);
        void addSection(std::string& str, const KeyValueList& list);
        void SplitTokens(const char* strptr, const char* delimeter, std::string& left, std::string& right);
        std::map<std::string, KeyValueList> _ConfigurationMap;
        std::string _ConfigFileName;
    };


#ifdef __cplusplus
}
#endif

#endif
