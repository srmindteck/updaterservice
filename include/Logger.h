#ifndef LOGGER
#define LOGGER

#include <map>
#include <string>
#include <fstream>
#include <chrono>
#include <filesystem>
#include <sstream>
#include <iostream>
#include <ctime>
#include <vector>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

    typedef enum LogLevel
    {
        LOG_INFO = 0,
        LOG_ERROR = 1,
        LOG_WARNING = 2,
        LOG_CRITICAL = 3,
        LOG_PANIC = 4
    }LogLevel;

    typedef enum LogFileMode
    {
        FILE_APPEND = 0,
        FILE_CREATE_NEW = 1
    }LogFileMode;

    class Logger
    {
    public:
        virtual ~Logger();
        void    StartLogging(LogFileMode fmode);
        void    StopLogging();
        void    Write(std::string logEntry, LogLevel llevel, const char* func, const char* file, int line);
        void    SetLogFileSize(int flsz);
        void    SetLogDirectory(std::string dirpath);
        void    SetModuleName(const char* mname);
        static Logger* GetInstance();
    private:
        Logger();
        void Logger::GetTimestamp(std::string& str);
        void createBackupFileName(std::string& str);
        void SplitTokens(const char* strptr, const char* delimeter, std::string &left, std::string &right);
        std::string logfilename;
        std::string  logDirectory;
        std::string  logBackupDirectory;
        int     logFileSize;
        std::string  moduleName;
        std::fstream logFile;
        LogFileMode filemode;
        std::map<LogLevel, std::string> logLevelMap;
    };

#define writeLog(str,level) Logger::GetInstance()->Write(str, level, __FUNCTION__, __FILE__, __LINE__);
#define writeLogNormal(str) Logger::GetInstance()->Write(str, LOG_INFO, __FUNCTION__, __FILE__, __LINE__);


#ifdef __cplusplus
}
#endif

#endif