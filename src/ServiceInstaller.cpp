#include <Windows.h>
#include <wincred.h>
#include <Dbt.h>
#include <list>
#include <string>

#include <tchar.h>
#include "ServiceInstaller.h"
#include "UpdaterService.h"
#include "ApplicationConstants.h"

BOOL WINAPI InstallUpdaterService()
{
    printf("Installing UpdaterService\n");

    TCHAR szFileName[MAX_PATH];
    GetModuleFileName(NULL, szFileName, MAX_PATH);

    SC_HANDLE schService;
    SC_HANDLE schSCManager;

    printf("Opening service manager\n");
    schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

    if (schSCManager == NULL)
    {
        printf("Could not open service manager\n");
        return false;
    }

    LPCTSTR lpszBinaryPathName = szFileName;

    printf("Creating service\n");

    schService = CreateService(schSCManager, SERVICE_NAME,
        SERVICE_DESCRIPTION, // service name to display
        SERVICE_ALL_ACCESS, // desired access 
        SERVICE_WIN32_OWN_PROCESS, // service type 
        SERVICE_DEMAND_START, // start type 
        SERVICE_ERROR_NORMAL, // error control type 
        lpszBinaryPathName, // service's binary 
        NULL, // no load ordering group 
        NULL, // no tag identifier 
        NULL, // no dependencies
        NULL, // LocalSystem account
        NULL); // no password

    if (schService == NULL)
    {
        LogMessage("Could not create service");
        printf("Could not create service");
        return false;
    }

    printf("Closing service manager\n");
    CloseServiceHandle(schService);

    return true;
}

BOOL WINAPI DeleteUpdaterService()
{
    printf("Removing UpdaterService\n");

    SC_HANDLE schSCManager;
    SC_HANDLE hService;
    schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

    if (schSCManager == NULL)
    {
        printf("Could not open service manager\n");
        return false;
    }

    hService = OpenService(schSCManager, SERVICE_NAME, SERVICE_ALL_ACCESS);

    if (hService == NULL)
    {
        printf("Could not open service\n");
        return false;
    }

    if (DeleteService(hService) == 0)
    {
        printf("Could not remove service\n");
        return false;
    }

    if (CloseServiceHandle(hService) == 0)
    {
        printf("Cloud not close service handle\n");
        return false;
    }

    return true;
}

BOOL WINAPI VerifyCredentials()
{
    return TRUE;

    CREDUI_INFO cui;
    TCHAR pszName[CREDUI_MAX_USERNAME_LENGTH + 1];
    TCHAR pszPwd[CREDUI_MAX_PASSWORD_LENGTH + 1];
    BOOL fSave;
    DWORD dwErr;

    cui.cbSize = sizeof(CREDUI_INFO);
    cui.hwndParent = NULL;
    //  Ensure that MessageText and CaptionText identify what credentials
    //  to use and which application requires them.
    cui.pszMessageText = TEXT("Enter administrator account information");
    cui.pszCaptionText = TEXT("Cascination Software Updater");
    cui.hbmBanner = NULL;
    fSave = FALSE;
    SecureZeroMemory(pszName, sizeof(pszName));
    SecureZeroMemory(pszPwd, sizeof(pszPwd));
    dwErr = CredUIPromptForCredentials(
        &cui,                         // CREDUI_INFO structure
        TEXT("TheServer"),            // Target for credentials
                                      //   (usually a server)
        NULL,                         // Reserved
        0,                            // Reason
        pszName,                      // User name
        CREDUI_MAX_USERNAME_LENGTH + 1, // Max number of char for user name
        pszPwd,                       // Password
        CREDUI_MAX_PASSWORD_LENGTH + 1, // Max number of char for password
        &fSave,                       // State of save check box
        CREDUI_FLAGS_GENERIC_CREDENTIALS |  // flags
        CREDUI_FLAGS_ALWAYS_SHOW_UI |
        CREDUI_FLAGS_DO_NOT_PERSIST);

    if (!dwErr)
    {
        //  Put code that uses the credentials here.

        //  When you have finished using the credentials,
        //  erase them from memory.
        SecureZeroMemory(pszName, sizeof(pszName));
        SecureZeroMemory(pszPwd, sizeof(pszPwd));
    }

    return TRUE;
}
