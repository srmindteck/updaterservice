#include "Logger.h"

Logger* objLogger = nullptr;

Logger* Logger::GetInstance()
{
    if (objLogger == nullptr)
    {
        objLogger = new Logger();
    }

    return objLogger;
}

Logger::Logger()
{
    logDirectory = "";
    logFileSize = 1024;

    logLevelMap.clear();

    logLevelMap[LOG_INFO] =     "Information";
    logLevelMap[LOG_WARNING] =  "Warning    ";
    logLevelMap[LOG_ERROR] =    "Error      ";
    logLevelMap[LOG_CRITICAL] = "Critical   ";
    logLevelMap[LOG_PANIC] =    "Panic      ";
}

Logger::~Logger()
{
    StopLogging();
}

void Logger::StopLogging()
{
    if (logFile.is_open())
    {
        logFile.flush();
        logFile.close();
    }
    logLevelMap.clear();
}

void Logger::createBackupFileName(std::string& str)
{
    std::string tstamp;
    GetTimestamp(tstamp);
    char temp[1024];
    memset((char*)&temp[0], 0, 16);
    sprintf(temp, "%s_%s.log", moduleName.c_str(), tstamp.c_str());
    str = temp;
}

void Logger::StartLogging(LogFileMode fmode)
{
    filemode = fmode;
    if (logDirectory.empty() || logDirectory.length() < 1)
    {
        std::string curdir = std::filesystem::current_path().string();
        curdir += "\\Logs\\";
        std::filesystem::create_directories(curdir);
        logDirectory = curdir;
    }

    logfilename = logDirectory + moduleName + ".log";

    logFile.open(logfilename, std::fstream::out);
}

void Logger::Write(std::string logEntry, LogLevel llevel, const char* func, const char* file, int line)
{
    if (logFile.good() && logFile.is_open())
    {
        size_t sz = logFile.tellg();

        if (sz >= logFileSize * 1024)
        {
            std::string temp;
            createBackupFileName(temp);
            std::string backupfile = logBackupDirectory + temp;
            StopLogging();
            int res = rename(logfilename.c_str(), backupfile.c_str());
            StartLogging(filemode);
        }

        logFile.seekg(0, std::ios::end);

        std::string sourcefile = std::filesystem::path(file).filename().string();
        std::string lvel = logLevelMap[llevel];

        std::string tstamp;
        GetTimestamp(tstamp);
        char temp[1024];
        memset((char*)&temp[0], 0, 16);

        char fname[256] = { 0 };
        memcpy(fname, func, 255);

        std::string left, right;

        SplitTokens(fname, "::", left, right);

        if (right.length() > 1)
        {
            strcpy(fname, right.c_str());
        }

        SplitTokens(fname, " ", left, right);

        if (right.length() > 1)
        {
            strcpy(fname, right.c_str());
        }

        sprintf(temp, "%s|%s|%05d|%s|%s| ", tstamp.c_str(), lvel.c_str(), line, fname, sourcefile.c_str());

        logEntry = temp + logEntry;
        logFile.write(logEntry.c_str(), logEntry.length());
        logFile.flush();
    }
}

void Logger::SetModuleName(const char* mname)
{
    moduleName = mname;
}

void Logger::SetLogFileSize(int flsz)
{
    logFileSize = flsz;
}

void Logger::SetLogDirectory(std::string dirpath)
{
    logDirectory = dirpath;

    std::filesystem::create_directories(logDirectory);
}

void Logger::GetTimestamp(std::string &strdatetime)
{
    const char* format = "%Y.%m.%d-%H:%M:%S";
    char buffer[256]= {0};

    struct tm timeinfo;
    time_t rawtime;
    time(&rawtime);
    timeinfo = *localtime(&rawtime);

    if (timeinfo.tm_year < 100)
    {
        timeinfo.tm_year += 100;
    }

    strftime(buffer, 256, format, &timeinfo);

    strdatetime = buffer;
}

void Logger::SplitTokens(const char* strptr, const char* delimeter, std::string& left, std::string& right)
{
    int ctr = 0;
    std::string str = strptr;
    // Skip delimiters at beginning
    std::string::size_type lastPos = str.find_first_not_of(delimeter, 0);

    // Find first non-delimiter
    std::string::size_type pos = str.find_first_of(delimeter, lastPos);

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
        if (ctr == 0)
        {
            left = str.substr(lastPos, pos - lastPos);
            ctr++;
            continue;
        }

        if (ctr == 1)
        {
            right = str.substr(lastPos, pos - lastPos);
            break;
        }        

        // Skip delimiters
        lastPos = str.find_first_not_of(delimeter, pos);
        // Find next non-delimiter
        pos = str.find_first_of(delimeter, lastPos);
    }
}
