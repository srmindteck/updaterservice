#include "Configuration.h"

Configuration* objConfiguration = nullptr;

const std::string WHITESPACE = " \n\r\t\f\v";

std::string ltrim(const std::string& s)
{
    size_t start = s.find_first_not_of(WHITESPACE);
    return (start == std::string::npos) ? "" : s.substr(start);
}

std::string rtrim(const std::string& s)
{
    size_t end = s.find_last_not_of(WHITESPACE);
    return (end == std::string::npos) ? "" : s.substr(0, end + 1);
}

std::string trim(const std::string& s) {
    return rtrim(ltrim(s));
}

Configuration::Configuration()
{
    _ConfigFileName = "";
}

Configuration::~Configuration()
{
}

Configuration* Configuration::GetInstance()
{
    if (objConfiguration == nullptr)
    {
        objConfiguration = new Configuration();
    }

    return objConfiguration;
}

void Configuration::setFileName(std::string fname)
{
    _ConfigFileName = fname;
}

bool Configuration::isSection(const std::string& section)
{
    std::map<std::string, KeyValueList>::const_iterator confmapiter;

    confmapiter = _ConfigurationMap.find(section);
    if (confmapiter == _ConfigurationMap.end())
    {
        return false;
    }
    else
    {
        return true;
    }
}

std::string Configuration::getValue(const std::string& section, const std::string& settingKey, const std::string defval)
{
    std::map<std::string, KeyValueList>::const_iterator confmapiter;
    KeyValueList::const_iterator kviter;
    std::string str;

    confmapiter = _ConfigurationMap.find(section);
    if (confmapiter == _ConfigurationMap.end())
    {
        return defval;
    }
    else
    {
        KeyValueList list = confmapiter->second;
        kviter = list.find(settingKey);

        if (kviter == list.end())
        {
            return defval;
        }
        str = (std::string)kviter->second;
    }
    return str;
}

bool Configuration::loadConfiguration()
{
    std::string curfilepath = std::filesystem::current_path().string();
    curfilepath += "\\Config\\";
    curfilepath += _ConfigFileName;
    curfilepath += ".ini";

    if (!loadConfiguration(curfilepath))
    {
        return false;
    }    
        
    return true;
}

bool Configuration::loadConfiguration(const std::string& configFile)
{
    std::ifstream cfgfile(configFile.c_str());
    std::string line, leftstr, rightstr;
    std::vector<std::string> linelist;

    // Following is a Windows INI style configuration file parsing algorithm
    // The first iteration only loads relevent lines from as a list of strings
    if (!cfgfile.is_open())
    {
        return false;
    }
    else
    {
        while (cfgfile.good())
        {
            line.erase();
            std::getline(cfgfile, line);
            line = trim(line);

            if (line.length() < 1 || line[0] == ';' || line[0] == '#' || line.empty())
            {
                //Skip comment or blank lines;
                continue;
            }

            if (!isalnum(line[0]))
            {
                if (line[0] == '[' && line[line.length() - 1] == ']')
                {
                    //Section header
                    linelist.push_back(line);
                }
                //Garbage or Invalid line
                continue;
            }
            else
            {
                //Normal line
                linelist.push_back(line);
            }
        }
        // The file can be closed off
        cfgfile.close();
    }

    //Now we would iterate the string list and segregate key value pairs by section groups
    std::string curSecHeader = "";
    KeyValueList kvlist;

    for (std::vector<std::string>::size_type i = 0; i != linelist.size(); i++)
    {
        line = linelist[i];
        //Section header line
        if (line[0] == '[' && line[line.length() - 1] == ']')
        {
            //Check whether this is the first instance of a section header
            if (_ConfigurationMap.size() < 1)
            {
                //Don't need to do anything
                if (curSecHeader.length() < 1)
                {
                }
                else
                {
                    //We reach here when a section is being read for the first time
                    addSection(curSecHeader, kvlist);
                }
            }
            else
            {
                //Before staring a new section parsing we need to store the last one
                addSection(curSecHeader, kvlist);
            }

            //Store the string as current section header and clear the key value list
            curSecHeader = line;
            kvlist.clear();
        }
        else
        {
            leftstr = rightstr = "";
            SplitTokens(line.c_str(), "=", leftstr, rightstr);
            kvlist[leftstr] = rightstr;
        }
    }
    addSection(curSecHeader, kvlist);
    return true;
}

void Configuration::addSection(std::string& str, const KeyValueList& list)
{
    str.erase(0, 1);
    str.erase(str.length() - 1, 1);
    _ConfigurationMap[str] = list;

}

void Configuration::SplitTokens(const char* strptr, const char* delimeter, std::string& left, std::string& right)
{
    int ctr = 0;
    std::string str = strptr;
    // Skip delimiters at beginning
    std::string::size_type lastPos = str.find_first_not_of(delimeter, 0);

    // Find first non-delimiter
    std::string::size_type pos = str.find_first_of(delimeter, lastPos);

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
        if (ctr == 0)
        {
            left = str.substr(lastPos, pos - lastPos);
            ctr++;
            continue;
        }

        if (ctr == 1)
        {
            right = str.substr(lastPos, pos - lastPos);
            break;
        }

        // Skip delimiters
        lastPos = str.find_first_not_of(delimeter, pos);
        // Find next non-delimiter
        pos = str.find_first_of(delimeter, lastPos);
    }
}
