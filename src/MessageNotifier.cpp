#include <Windows.h>
#include <tchar.h>
#include <string.h>

#include "UpdaterService.h"
#include "MessageNotifier.h"

#define BUFFER_SIZE 1024

HANDLE hMapFile;
TCHAR ipcName[] = TEXT("SoftwareUpdaterView");
LPCTSTR messageBuffer;

MessageNotifier* objMessageNotifier = nullptr;

MessageNotifier::MessageNotifier()
{

}

MessageNotifier::~MessageNotifier()
{

}

MessageNotifier* MessageNotifier::GetInstance()
{
    if (objMessageNotifier == nullptr)
    {
        objMessageNotifier = new MessageNotifier();
    }

    return objMessageNotifier;
}

bool MessageNotifier::OpenNotifier()
{
    hMapFile = CreateFileMappingA(
        INVALID_HANDLE_VALUE,    // use paging file
        NULL,                    // default security
        PAGE_READWRITE,          // read/write access
        0,                       // maximum object size (high-order DWORD)
        BUFFER_SIZE,             // maximum object size (low-order DWORD)
        ipcName);                // name of mapping object

    if (hMapFile == NULL)
    {
        LogMessage("Could not create file mapping object");
        return false;
    }

    messageBuffer = (LPTSTR)MapViewOfFile(hMapFile,     // handle to map object
        FILE_MAP_ALL_ACCESS,                            // read/write permission
        0,
        0,
        BUFFER_SIZE);

    if (messageBuffer == NULL)
    {
        CloseHandle(hMapFile);
        LogMessage("Could not map view of file");
        return false;
    }

	return true;
}

bool MessageNotifier::CloseNotifier()
{
    UnmapViewOfFile(messageBuffer);
    CloseHandle(hMapFile);

	return false;
}

bool MessageNotifier::IsOpen()
{
	return false;
}

bool MessageNotifier::SendNotification(const char* lptrmessage)
{
    std::string data(lptrmessage);
    std::wstring wsdata(data.begin(), data.end());

    DWORD size = (DWORD)wsdata.size() + 2;
    CopyMemory((PVOID)messageBuffer, wsdata.c_str(), size*2);

    LogMessage(lptrmessage);
    return false;
}