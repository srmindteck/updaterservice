#include "AdapterLoader.h"

AdapterLoader* objAdapter = nullptr;

AdapterLoader::AdapterLoader()
{

}

AdapterLoader::~AdapterLoader()
{

}

AdapterLoader* AdapterLoader::GetInstance()
{
    if (objAdapter == nullptr)
    {
        objAdapter = new AdapterLoader();
    }

    return objAdapter;
}

unsigned long AdapterLoader::LoadAdapters()
{
	//Get the current directory
    std::string curdir = std::filesystem::current_path().string();
	//Our DLLs will be found in the 'adapters' directory
    curdir += "\\adapters";

	std::list<std::string> filelist;
	GetDllList(curdir.c_str(), filelist);

    for(std::string str : filelist)
    {
        SoftwareUpdaterAdapter* updater_adapter = new SoftwareUpdaterAdapter();

        updater_adapter->adapterlib = LoadLibrary(str.c_str());
        if (updater_adapter->adapterlib == NULL)
        {
            delete updater_adapter;
            continue;
        }

        updater_adapter->load = (fn_load)GetProcAddress(updater_adapter->adapterlib, "adapter_load");
        if (!updater_adapter->load)
        {
            FreeLibrary(updater_adapter->adapterlib);
            delete updater_adapter;
            continue;
        }

        updater_adapter->unload = (fn_unload)GetProcAddress(updater_adapter->adapterlib, "adapter_unload");
        if (!updater_adapter->unload)
        {
            FreeLibrary(updater_adapter->adapterlib);            
            delete updater_adapter;
            continue;
        }

        updater_adapter->set_security = (fn_set_security)GetProcAddress(updater_adapter->adapterlib, "adapter_set_security");
        if (!updater_adapter->set_security)
        {
            FreeLibrary(updater_adapter->adapterlib);
            delete updater_adapter;
            continue;
        }

        updater_adapter->load_from_buffer = (fn_load_from_buffer)GetProcAddress(updater_adapter->adapterlib, "adapter_software_update_load_from_buffer");
        if (!updater_adapter->load_from_buffer)
        {
            FreeLibrary(updater_adapter->adapterlib);
            delete updater_adapter;
            continue;
        }

        updater_adapter->load_from_dirpath = (fn_load_from_dirpath)GetProcAddress(updater_adapter->adapterlib, "adapter_software_update_load_from_dirpath");
        if (!updater_adapter->load_from_dirpath)
        {
            FreeLibrary(updater_adapter->adapterlib);
            delete updater_adapter;
            continue;
        }

        updater_adapter->apply = (fn_apply)GetProcAddress(updater_adapter->adapterlib, "adapter_software_update_apply");
        if (!updater_adapter->apply)
        {
            FreeLibrary(updater_adapter->adapterlib);
            delete updater_adapter;
            continue;
        }

        updater_adapter->rollback = (fn_rollback)GetProcAddress(updater_adapter->adapterlib, "adapter_software_update_rollback");
        if (!updater_adapter->rollback)
        {
            FreeLibrary(updater_adapter->adapterlib);
            delete updater_adapter;
            continue;
        }

        updater_adapter->identity = (fn_get_identity)GetProcAddress(updater_adapter->adapterlib, "adapter_get_identity");
        if (!updater_adapter->identity)
        {
            FreeLibrary(updater_adapter->adapterlib);
            delete updater_adapter;
            continue;
        }
        else
        {
            const char* temp = updater_adapter->identity(nullptr);
            updater_adapter->identitystr = new char[strlen(temp) + 1];
            memset(updater_adapter->identitystr, 0, strlen(temp) + 1);
            memcpy(updater_adapter->identitystr, temp, strlen(temp));
        }

        updater_adapter->pre_shared_key = (fn_get_pre_shared_key)GetProcAddress(updater_adapter->adapterlib, "adapter_get_pre_shared_key");
        if (!updater_adapter->pre_shared_key)
        {
            FreeLibrary(updater_adapter->adapterlib);
            if (updater_adapter->identitystr)
            {
                delete updater_adapter->identitystr;
            }
            delete updater_adapter;
            continue;
        }
        else
        {
            const AdapterKey* temp = updater_adapter->pre_shared_key(nullptr);
            updater_adapter->keyptr = new AdapterKey;
            updater_adapter->keyptr->BufferSize = temp->BufferSize;
            updater_adapter->keyptr->Buffer = new unsigned char[temp->BufferSize];
            memset(updater_adapter->keyptr->Buffer, 0, updater_adapter->keyptr->BufferSize);
            memcpy(updater_adapter->keyptr->Buffer, temp->Buffer, updater_adapter->keyptr->BufferSize);
        }

        adapterList[updater_adapter->identitystr] = updater_adapter;
    }

	return adapterList.size();
}

unsigned long AdapterLoader::UnloadAdapters()
{
	return -1;
}

SoftwareUpdaterAdapter* AdapterLoader::SelectAdapter(std::string& str)
{
	return nullptr;
}

void AdapterLoader::ReleaseCurrentAdapter(SoftwareUpdaterAdapter* ptr)
{
	if (ptr)
	{
		if (ptr->adapterlib)
		{
			FreeLibrary(ptr->adapterlib);
			ptr->adapterlib = NULL;
		}

		delete ptr;
		ptr = nullptr;
	}
}

void AdapterLoader::GetDllList(const char* dirpath, std::list<std::string> &filelist)
{
    for (auto const& dir_entry : std::filesystem::directory_iterator{ dirpath })
    {
        if (dir_entry.is_regular_file())
        {
            std::string ext = dir_entry.path().extension().string();
            if (dir_entry.path().extension().string() == ".DLL" || dir_entry.path().extension().string() == ".dll")
            {
                filelist.push_back(dir_entry.path().string());
            }
        }
    }
}

void AdapterLoader::OnAdapterNotification(const AdapterNotification* notification)
{

}