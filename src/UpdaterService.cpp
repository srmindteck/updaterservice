#include <Windows.h>
#include <wincred.h>
#include <Dbt.h>
#include <shlobj.h>

#include <list>
#include <string>

#include <tchar.h>
#include "UpdaterService.h"
#include "ServiceInstaller.h"
#include "MessageNotifier.h"
#include "AdapterLoader.h"
#include "Logger.h"
#include "Configuration.h"
#include "ApplicationConstants.h"

SERVICE_STATUS          serviceStatus = {0};
SERVICE_STATUS_HANDLE   serviceStatusHandle = NULL;
HANDLE                  serviceStopEventHandle = INVALID_HANDLE_VALUE;

std::list<std::string> fixedDriveList;
std::list<std::string> removableDriveList;

FILE* fp = NULL;

int _tmain (int argc, TCHAR *argv[])
{
    //Service installation workflow
    if (argc > 1 && strcmp(argv[1], "-i") == 0)
    {
        if (InstallUpdaterService())
        {
            LogMessage("Service Installed Sucessfully");
            return 0;
        }
        else
        {
            LogMessage("Error Installing Service");
            return -1;
        }        
    }

    //Service uninstallation workflow
    if (argc > 1 && strcmp(argv[1], "-u") == 0)
    {
        if (DeleteUpdaterService())
        {
            LogMessage("Service UnInstalled Sucessfully");
        }
        else
        {
            LogMessage("Error UnInstalling Service");
        }
    }

    // Service Debugging workflow
    if (argc > 1 && strcmp(argv[1], "-d") == 0)
    {
        DebugMain(argc, argv);
        return 0;
    }

    //If the flow has come to this line then this means that the program was invoked with invalid command line
    if (argc > 1)
    {
        LogMessage("\n\nUnknown Switch Usage\n\nFor Install use UpdaterService.exe -i\n\nFor UnInstall use UpdaterService.exe -u\n");
        return -1;
    }
    
    //If the program was called without any switch then this means normal service start is requested

    fp = fopen("D:\\SoftwareUpdaterService.log", "w");

    SERVICE_TABLE_ENTRY ServiceTable[] =
    {
        {SERVICE_NAME, (LPSERVICE_MAIN_FUNCTION)ServiceMain},
        {NULL, NULL}
    };

    if (StartServiceCtrlDispatcher(ServiceTable) == FALSE)
    {
        LogMessage("Updater Service: Main: StartServiceCtrlDispatcher returned error");
        return GetLastError();
    }

    if (fp)
    {
        fflush(fp);
        fclose(fp);
    }

    return 0;
}

VOID WINAPI ServiceMain (DWORD argc, LPTSTR *argv)
{
    LogMessage("Service Entry");
    DWORD Status = E_FAIL;

    PreLoadDisks();

    // Create stop event to wait on later.
    serviceStopEventHandle = CreateEvent(NULL, TRUE, FALSE, NULL);

    if (serviceStopEventHandle == NULL)
    {
        LogMessage("Updater Service: ServiceMain: CreateEvent(handlearray[SERVICE_STOP_HANDLE]) returned error");
        return;
    }

    DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;
    ZeroMemory(&NotificationFilter, sizeof(NotificationFilter));
    NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
    NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;

    serviceStatusHandle = RegisterServiceCtrlHandlerExA(SERVICE_NAME, ServiceCtrlHandler, NULL);

    HDEVNOTIFY m_hDevNotify = RegisterDeviceNotificationA(serviceStatusHandle,
        &NotificationFilter, DEVICE_NOTIFY_SERVICE_HANDLE |
        DEVICE_NOTIFY_ALL_INTERFACE_CLASSES);   
     
    if (serviceStatusHandle == NULL) 
    {
        LogMessage("Updater Service: ServiceMain: RegisterServiceCtrlHandler returned error");
    }
    else
    {
        // Tell the service controller we are starting
        ZeroMemory(&serviceStatus, sizeof(serviceStatus));
        serviceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
        serviceStatus.dwControlsAccepted = 0;
        serviceStatus.dwCurrentState = SERVICE_START_PENDING;
        serviceStatus.dwWin32ExitCode = 0;
        serviceStatus.dwServiceSpecificExitCode = 0;
        serviceStatus.dwCheckPoint = 0;

        if (SetServiceStatus(serviceStatusHandle, &serviceStatus) == FALSE)
        {
            LogMessage("Updater Service : ServiceMain: SetServiceStatus returned error");
        }

        // Perform tasks neccesary to start the service here
        LogMessage("Performing Service Start Operations");

        // Tell the service controller we have started
        serviceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
        serviceStatus.dwCurrentState = SERVICE_RUNNING;
        serviceStatus.dwWin32ExitCode = 0;
        serviceStatus.dwCheckPoint = 0;

        if (SetServiceStatus(serviceStatusHandle, &serviceStatus) == FALSE)
        {
            LogMessage("Updater Service: ServiceMain: SetServiceStatus returned error");
        }
      
        LogMessage("Waiting Stop Event");
        WaitForSingleObject(serviceStopEventHandle, INFINITE);
        LogMessage("Received Stop Event");

            // Perform any cleanup tasks
        LogMessage("Performing Cleanup Operations");

        CloseHandle(serviceStopEventHandle);

        serviceStatus.dwControlsAccepted = 0;
        serviceStatus.dwCurrentState = SERVICE_STOPPED;
        serviceStatus.dwWin32ExitCode = 0;
        serviceStatus.dwCheckPoint = 3;

        if (SetServiceStatus(serviceStatusHandle, &serviceStatus) == FALSE)
        {
            LogMessage("Updater Service: ServiceMain: SetServiceStatus returned error");
        }
    }

    LogMessage("Service Exit");

    return;
}

VOID WINAPI DebugMain(DWORD argc, LPTSTR* argv)
{
    PreLoadDisks();

    std::filesystem::path path;
    PWSTR path_tmp;

    auto get_folder_path_ret = SHGetKnownFolderPath(FOLDERID_RoamingAppData, 0, nullptr, &path_tmp);

    if (get_folder_path_ret != S_OK) 
    {
        CoTaskMemFree(path_tmp);
        return;
    }

    path = path_tmp;

    CoTaskMemFree(path_tmp);

    std::string logdir = path.string();
    logdir += "\\";
    logdir += ORGANIZATION_NAME;
    logdir += "\\";
    logdir += APPLICATION_NAME;
    logdir += "\\";
    logdir += "logs";
    logdir += "\\";

    std::string configdir = path.string();
    configdir += "\\";
    configdir += ORGANIZATION_NAME;
    configdir += "\\";
    configdir += APPLICATION_NAME;
    configdir += "\\";
    configdir += "config";
    configdir += "\\";

    Logger* logger = Logger::GetInstance();

    logger->SetModuleName(APPLICATION_NAME);
    logger->SetLogDirectory(logdir);
    logger->StartLogging(FILE_CREATE_NEW);
    writeLogNormal("Start of debugging");

    MessageNotifier* notifier = MessageNotifier::GetInstance();

    notifier->OpenNotifier();

    Configuration* config = Configuration::GetInstance();

    AdapterLoader* loader = AdapterLoader::GetInstance();

    loader->LoadAdapters();
}

DWORD WINAPI ServiceCtrlHandler (DWORD CtrlCode, DWORD evtype, PVOID evdata, PVOID context)
{
    switch (CtrlCode) 
	{
        case SERVICE_CONTROL_STOP:
        {
            if (serviceStatus.dwCurrentState != SERVICE_RUNNING)
            {
                break;
            }

            // Perform tasks neccesary to stop the service here        
            serviceStatus.dwControlsAccepted = 0;
            serviceStatus.dwCurrentState = SERVICE_STOP_PENDING;
            serviceStatus.dwWin32ExitCode = 0;
            serviceStatus.dwCheckPoint = 4;

            if (SetServiceStatus(serviceStatusHandle, &serviceStatus) == FALSE)
            {
                LogMessage("Updater Service: ServiceCtrlHandler: SetServiceStatus returned error");
            }

            // This will signal the main thread to start shutting down
            SetEvent(serviceStopEventHandle);
            LogMessage("Setting stop event");
        }
        break;

     case SERVICE_CONTROL_DEVICEEVENT:
         {
             DeviceEventNotify(evtype, evdata, context);
         }
         break;
     
     default:
         break;
    }

    return 0;
}

DWORD WINAPI DeviceEventNotify(DWORD evtype, PVOID evdata, PVOID context)
{
    if (evdata)
    {
        char str[32] = { 0 };
        PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)evdata;
        PDEV_BROADCAST_VOLUME lpdbv = (PDEV_BROADCAST_VOLUME)lpdb;

        char drive = FirstDriveFromMask(lpdbv->dbcv_unitmask);

        std::string drivestr;
        drivestr += drive;
        drivestr += ":\\";

        UINT nDriveType = GetDriveTypeA(drivestr.c_str());

        if (nDriveType == DRIVE_REMOVABLE)
        {
            switch (evtype)
            {
                case DBT_DEVICEREMOVECOMPLETE:
                {
                    sprintf(str, "Removed drive : %c:", drive);
                }
                break;
                case DBT_DEVICEARRIVAL:
                {
                    sprintf(str, "Inserted drive : %c:", drive);
                }
                break;
            }

            LogMessage(str);
        }
    }

    return 0;
}

VOID WINAPI LogMessage(const char* message)
{
    if (fp)
    {
        fprintf(fp, "%s\n", message);
        fflush(fp);
    }

    if (Logger::GetInstance() != nullptr)
    {
        writeLogNormal(message);
    }
}

char FirstDriveFromMask(ULONG unitmask)
{
    char i;

    for (i = 0; i < 26; ++i)
    {
        if (unitmask & 0x1)
            break;
        unitmask = unitmask >> 1;
    }

    return(i + 'A');
}

void PreLoadDisks()
{
    int count = 0;

    char szLogicalDrives[MAX_PATH] = {0};
    char text[MAX_PATH + 1];
    char driveletter = 0;

    DWORD dwResult = GetLogicalDriveStringsA(MAX_PATH, text);
    char* szSingleDrive = text;

    while (*szSingleDrive)
    {
        UINT nDriveType = GetDriveTypeA(szSingleDrive);

        switch (nDriveType)
        {
            case DRIVE_REMOVABLE:
            {
                removableDriveList.push_back(szSingleDrive);
                break;
            }
            case DRIVE_FIXED:
            case DRIVE_CDROM:
            case DRIVE_REMOTE:
            case DRIVE_RAMDISK:
            {
                fixedDriveList.push_back(szSingleDrive);
                break;
            }
            default:
            {
                break;
            }
        }

        szSingleDrive += strlen(szSingleDrive) + 1; // next drive 
    }
}
